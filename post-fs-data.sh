#!/system/bin/sh
# Please don't hardcode /magisk/modname/... ; instead, please use $MODDIR/...
# This will make your scripts compatible even if Magisk change its mount point in the future
MODDIR=${0%/*}

# This script will be executed in post-fs-data mode
# More info in https://github.com/topjohnwu/Magisk/blob/master/docs/details.md

DIR=/system/etc/permissions
FILE=$DIR/handheld_core_hardware.xml

if [ ! -f $FILE ]; then
    # Try Treble
    DIR=/vendor/etc/permissions
    FILE=$DIR/handheld_core_hardware.xml
fi

if [ ! -f $FILE ]; then
	echo "ERROR: Could not find handheld_core_hardware.xml"
fi

cd  ${MODDIR}${DIR}
cp -a ${FILE} ./
sed -ri 's,<permissions>,<permissions>\n<feature name="android.software.vr.mode" />\n<feature name="android.hardware.vr.high_performance" />,g' handheld_core_hardware.xml
